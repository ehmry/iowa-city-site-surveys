# Mesh line-of-sight estimations

This repository contains data for rough line-of-sight analysis of radio sites.
To analyze a site, create a '*.qth' file and run the `tup` utility in the repo.

The current analysis parameters are very rough guesses and over-estimates,
please consult the documentation of SPLAT! before tweaking them.

## Dependecies
* [Tup](https://gittup.org/tup)
* [SPLAT!](https://www.qsl.net/kd2bd/splat.html)
* [GraphicsMagick](http://www.graphicsmagick.org/)

## TODO
- Use recent LIDAR [data](http://www.geotree.uni.edu/en/extension/iowa-lidar-mapping-project/)
for topography rather than the 30-meter resolution [SRTM](https://en.wikipedia.org/wiki/Shuttle_Radar_Topography_Mission) data.

## Example
![](./forest-view.png)
